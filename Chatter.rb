#####
# Copyright (C) 2016 Savoir-faire Linux, Inc.
# Licensed under the Apache License, Version 2.0
# (the "License"); you may not use this file except in compliance with the
# License. You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.
#
require 'ruby_expect'

module RubyExpect
  class Expect
    attr_reader :child_pid
  end
end


class Chatter
  SIGTERM = 15
  FAILED = 'failed'
  PASSED = 'ok'
  def initialize(debug, timeout, karg)
    @cond = ConditionVariable.new
    @mutex = Mutex.new
    @run = true
    @test = {}
    @connector = factory(debug, timeout, karg)
  end

  def stop
    @run=false
    @cond.signal
  end

  def connect
    Thread.new do
      pid =  @connector.child_pid
      while @run do
        @mutex.synchronize do
          @cond.wait(@mutex) while @run && (@test.empty? || !@test[:result].nil?)
          break unless @run

          @test[:result] = runner
          @test[:last_mach] = @connector.last_match
          @test[:buffer] = @connector.buffer
          @test[:before] = @connector.before
        end
      end
      begin
        Process.kill(SIGTERM, pid)
      rescue Errno::ESRCH
        # process exited normally
      end
    end
  end

  #SE
  def send_command(cmd, regex)
    @connector.send cmd
    result = FAILED
    @connector.expect regex do
      result = PASSED
    end
    result
  end

  def prompt_is_present(regex, force_prompt = false)
    result = if regex.match(@connector.match)
              true
             elsif force_prompt
               send_command('', regex)
             else
              false
             end
    result
  end

  def runner
    cmd = @test[:cmd]
    prompt = @test[:prompt]
    expected_from_cmd = @test[:expect]
    #force_prompt = @test[:force_prompt]
    result = if prompt_is_present(prompt) || @connector.expect(prompt)
      send_command(cmd, expected_from_cmd)
    end
  end

  def factory(debug, timeout, karg)
    connector = if karg[:socket]
                  RubyExpect::Expect.connect(karg[:socket])
                else
                  RubyExpect::Expect.spawn(karg[:command])
                end
    connector.debug = debug
    connector.timeout = timeout
    connector

  end

  def addTests(test)
    @mutex.synchronize do
      @test = test
    end
    @cond.signal
  end

  def getTest
    while @test[:result].nil?
      sleep 0.1 #no good to do spin-lock
    end
    @test
  end

  private :factory, :runner
end

